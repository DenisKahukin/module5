﻿using System;
using System.Collections.Generic;

namespace module5
{
    class Program
    {
        static void Main()
        {
            new Program().GamePreparathion();
        }

        public void GamePreparathion()
        {
            while (true)
            {
                new DisplayData().DisplayMessage("Select action 1 - Start game, 0 - Exit");
                if (int.TryParse(Console.ReadLine(), out int result))
                {
                    Action action = (Action)result;
                    switch (action)
                    {
                        case Action.StartGame:
                            StartGame();
                            break;
                        case Action.Exit:
                            return;
                    }
                }
                else
                {
                    new DisplayData().DisplayMessage("Action not recognized please repeat the action");
                }
            } 
        }
        public void StartGame()
        {
            int lives = 10, posithion = 0;
            List<PlayingField> playingFields = new SetData().SetPosithionAndChar();
            new SetData().SetTrap(ref playingFields);
            new DisplayData().DisplayPlayingField(playingFields, lives);
            while (true)
            {
                if(!new Check().CheckLivesMoreZero(ref lives))
                {
                    new DisplayData().DisplayMessage("You Lose!");
                    break;
                }
                if (new Check().CheckPlayerWin(posithion))
                {
                    new DisplayData().DisplayMessage("You Win!");
                    break;
                }
                    ProcessingAction(ref posithion, ref playingFields, ref lives);
            }
        }

        private void ProcessingAction(ref int posithion, ref List<PlayingField> playingFields, ref int lives)
        {
            int previousPosithion;
            Key acthion = new Check().CheckKey(GetConsoleKey());
            switch (acthion)
            {
                case Key.KeyW:
                    posithion -= 10;
                    if(new Check().CheckBorder(posithion))
                    {
                        new SetData().SetNewPosithion(posithion, posithion + 10, ref playingFields);
                        new SetData().SetLives(posithion, playingFields, ref lives);
                        new DisplayData().DisplayPlayingField(playingFields, lives);
                    }
                    else
                    {
                        new DisplayData().DisplayMessage(" Wrong action. Repeat action! ");
                        posithion += 10;
                    }
                    break;
                case Key.KeyS:
                    posithion += 10;
                    if (new Check().CheckBorder(posithion))
                    {
                        new SetData().SetNewPosithion(posithion, posithion - 10, ref playingFields);
                        new SetData().SetLives(posithion, playingFields, ref lives);
                        new DisplayData().DisplayPlayingField(playingFields, lives);
                    }
                    else
                    {
                        new DisplayData().DisplayMessage(" Wrong action. Repeat action! ");
                        posithion -= 10;
                    }
                    break;
                case Key.KeyD:
                    posithion++;
                    previousPosithion = posithion - 1;
                    if (new Check().CheckBorder(posithion) && new Check().CheckSideBorder(posithion, previousPosithion))
                    {
                        new SetData().SetNewPosithion(posithion, posithion - 1, ref playingFields);
                        new SetData().SetLives(posithion, playingFields, ref lives);
                        new DisplayData().DisplayPlayingField(playingFields, lives);
                    }
                    else
                    {
                        new DisplayData().DisplayMessage(" Wrong action. Repeat action! ");
                        posithion--;
                    }
                    break;
                case Key.KeyA:
                    posithion--;
                    previousPosithion = posithion + 1;
                    if (new Check().CheckBorder(posithion) && new Check().CheckSideBorder(posithion, previousPosithion))
                    {
                        new SetData().SetNewPosithion(posithion, posithion + 1, ref playingFields);
                        new SetData().SetLives(posithion, playingFields, ref lives);
                        new DisplayData().DisplayPlayingField(playingFields, lives);
                    }
                    else
                    {
                        new DisplayData().DisplayMessage(" Wrong action. Repeat action! ");
                        posithion++;
                    }
                    break;
                case Key.AnotherKey:
                    new DisplayData().DisplayMessage("You enter wrong key. Please repeat action");
                    break;
            }
        }




        public ConsoleKeyInfo GetConsoleKey()
        {
            ConsoleKeyInfo consoleKey;
            consoleKey = Console.ReadKey();
            return consoleKey;
        }
        
    } 
}
