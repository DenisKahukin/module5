﻿using System;
using System.Collections.Generic;

namespace module5
{
    class DisplayData
    {
        public void DisplayPlayingField(List<PlayingField> playingFields, int lives)
        {
            Console.Clear();
            for (int i = 0; i < playingFields.Count; i++)
            {
                if (i % 10 == 0 && i != 0)
                {
                    Console.WriteLine();
                }
                Console.Write(playingFields[i].DisplayChar + " ");
            }
            DisplayLives(lives);
        }
        public void DisplayMessage(string str)
        {
            Console.WriteLine(str);
        }
        public void DisplayLives(int lives)
        {
            Console.WriteLine($"\nYour lives: {lives.ToString()}");
        }
    }
}
