﻿namespace module5
{
    enum Key
    {
        KeyW = 1,
        KeyS,
        KeyD,
        KeyA,
        AnotherKey = -1,
    }
}
