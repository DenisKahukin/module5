﻿using System;
using System.Collections.Generic;

namespace module5
{
    class Check
    {
        public bool CheckHaveTrap(List<PlayingField> fields, int posithion)
        {
            if(fields[posithion].Damage == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public bool CheckBorder(int posithion)
        {
            if(posithion < 0 || posithion > 99)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool CheckSideBorder(int posithion, int oldPosithion)
        {
            if (posithion % 10 == 0 && oldPosithion % 9 == 0 || posithion % 9 == 0 && oldPosithion % 10 == 0)  
            {
                return false;
            }
            else { return true; }
        }
        public bool CheckTrap(List<PlayingField> fields, int posithion)
        {
            if(fields[posithion].Damage != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CheckLivesMoreZero(ref int lives)
        {
            if(lives > 0)
            {
                return true;
            }
            else { return false; }
        }
        public bool CheckPlayerWin(int posithion)
        {
            if (posithion == 99)
            {
                return true;
            }
            else { return false; }
        }
        public Key CheckKey(ConsoleKeyInfo consoleKey)
        {

            if (Console.Equals(consoleKey.Key, ConsoleKey.UpArrow) || Console.Equals(consoleKey.Key, ConsoleKey.W))
            {
                return Key.KeyW;
            }
            if (Console.Equals(consoleKey.Key, ConsoleKey.DownArrow) || Console.Equals(consoleKey.Key, ConsoleKey.S))
            {
                return Key.KeyS;
            }
            if (Console.Equals(consoleKey.Key, ConsoleKey.RightArrow) || Console.Equals(consoleKey.Key, ConsoleKey.D))
            {
                return Key.KeyD;
            }
            if (Console.Equals(consoleKey.Key, ConsoleKey.LeftArrow) || Console.Equals(consoleKey.Key, ConsoleKey.A))
            {
                return Key.KeyA;
            }
            return Key.AnotherKey;
        }
    }
}
