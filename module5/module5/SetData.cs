﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace module5
{
    class SetData
    {
        public List<PlayingField> SetPosithionAndChar()
        {
            List<PlayingField> playingFields = new List<PlayingField>();
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    PlayingField playingField = new PlayingField
                    {
                        PosithionX = i,
                        PosithionY = j
                    };
                    if (i == 0 && j == 0)
                    {
                        playingField.DisplayChar = 'I';
                    }
                    else
                    {
                        playingField.DisplayChar = '#';
                    }
                    playingFields.Add(playingField);
                }
            }
            return playingFields;
        }

        public void SetTrap(ref List<PlayingField> playingFields )
        {
            int i = 0;
            while(i < 10)
            {
                int posithion = new Random().Next(1, playingFields.Count -2);
                if(new Check().CheckHaveTrap(playingFields, posithion))
                {
                    playingFields[posithion].Damage = new Random().Next(1, 10);
                    i++;
                }
            }
        }
        public void SetLives(int posithion, List<PlayingField> fields ,ref int lives)
        {
            if(new Check().CheckTrap(fields, posithion))
            {
                lives -= fields[posithion].Damage;
                new DisplayData().DisplayMessage("The trap work be careful!");
                Thread.Sleep(1000);
            }
        }
        public void SetNewPosithion(int newPosithion, int oldPosithion, ref List<PlayingField> playingFields)
        {
            playingFields[oldPosithion].DisplayChar = '#';
            playingFields[newPosithion].DisplayChar = 'I';
        }
    }
}
